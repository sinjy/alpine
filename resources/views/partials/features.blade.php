<!--Section Caracteristiques-->
<section id="features">
    <div class="container mt-5">
        <div class="row features">
            <div class="col-12">
                <h1 class="text-center text-blue writing ">Caractéristiques<br> Techniques</h1>
                <table class="table table-hover table-dark mt-5">
                    <thead>
                    <tr class="bg-dark">
                        <th scope="col">
                            Puissance moteur
                        </th>
                        <th scope="col">
                            252 ch (185kw)
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="bg-primary">
                        <td>Accélération de 0 à 100 km/h
                        </td>
                        <td>4,5 secondes</td>
                    </tr>
                    <tr>
                        <td>Vitesse maximale (limitée éléctroniquement)</td>
                        <td>250 km/h</td>
                    </tr>
                    <tr class="bg-primary">
                        <td>Consommation en cycle mixte*</td>
                        <td>6,4 l/100</td>
                    </tr>
                    <tr>
                        <td>Émissions CO2 en cycle mixte*</td>
                        <td>144 g/km</td>
                    </tr>
                    <tr class="bg-primary">
                        <td>Boîte de vitesse</td>
                        <td>Automatique double embrayage à 7 rapports</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!--end of section-->