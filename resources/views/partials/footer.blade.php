    <style>
    footer {
        background-color: rgb(1, 190, 254);
        font-size: 18px;
        font-family: "alpine-air";
    }
    .fab {
        margin: 1%;
        color: white !important;
        float: right;
    }

    .footer-link:hover {
        background-color: white;
        color: rgb(1, 190, 254) !important;
        text-decoration: none;

    }
</style>


<footer class="fixed-bottom">
    <div class="container pb-2">
        <div class="row">
            <div class="col-5 mt-2 text-white">
                <a class="text-white footer-link" href="#">Nous contacter</a> |
                <a class="text-white footer-link" data-toggle="modal" data-target="#exampleModalLong" style="cursor:pointer;">Mentions légales</a> |
                <a class="text-white footer-link" href="#">Photos </a>
                <!-- Modal -->
                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-dark" id="exampleModalLongTitle">TROLL</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img src="https://img.20mn.fr/sIChN5W-TCG0VWSpGYJYLw/960x614_tous-trolls.jpg" width="250">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-7 mt-2">
                <a href="https://www.pinterest.fr/alpine_cars/"><i class="fab fa-pinterest fa-1x"></i></a>
                <a href="https://www.youtube.com/channel/UCzH4Iwlm8kI09wXbgHvFfIg"><i class="fab fa-youtube fa-1x"></i></a>
                <a href="https://www.facebook.com/alpinesportscars"><i class="fab fa-facebook-f fa-1x"></i></a>
                <a href="https://www.instagram.com/alpine_cars"><i class="fab fa-instagram fa-1x"></i></a>
                <a href="https://twitter.com/alpine_cars"><i class="fab fa-twitter fa-1x"></i></a>
                <a href="https://www.linkedin.com/company/alpine-cars"><i class="fab fa-linkedin-in fa-1x"></i></a>
            </div>
        </div>
    </div>










</footer>

