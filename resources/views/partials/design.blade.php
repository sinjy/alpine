 
 <!--Section Design-->
 <section id="design" class="bg-blue">
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-12 pt-5">
                <h1 class="text-center text-white writing">DESIGN</h1>
                <p class="lead">La nouvelle A110 combine des éléments de style de la mythique berlinette à
                    d'autres plus modernes à
                    l’instar des feux arrière LED en forme de « X » intégrant des clignotants à défilement. Emmenée par
                    Antony Villain, l’équipe de designers a su saisir l’esprit Alpine et créer un design élégant,
                    inspiré de la berlinette, mais qui résistera à l’épreuve du temps.</p>
                <hr>
            </div>
        <div class="row">
            <div class="col-sm-6 pt-3">
                <h3 class="text-white">Silhouette</h3>
                <p>L’Alpine A110, c’est avant tout un dessin tendu réalisé d’un seul trait. C’est aussi une peau qui
                    épouse au plus près les organes mécaniques, une assise et un centre de gravité abaissés, une vaste
                    lunette arrière galbée et des flancs creusés qui soulignent des passages de roues saillants.</p><br><br>
            </div>
            <div class="col-sm-6 pt-3">
                <h3 class="text-white ">Une carrosserie aluminium</h3>
                <p> L’aluminium est partout. Utilisé pour la carrosserie et le soubassement rivetés et soudés, ce métal
                    noble ultraléger assure à l'A110 une structure équilibrée et souple. Clins d'œil aux anciennes
                    Alpine : les flancs creusés et la nervure centrale sur le capot.</p>
            </div>
        </div>
    </div>
</section>
<!--end of section--