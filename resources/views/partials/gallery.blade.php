    <!--Section Gallerie-->
    <section id="gallery">
        <div class="container mt-5">
            <h1 class="text-center mb-5 text-blue writing">Galerie Photo</h1>
            <div class="row">
                <div class="col-6 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_1.jpg') }}" onclick="openModal();currentSlide(1)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-6 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_5.jpg') }}" onclick="openModal();currentSlide(2)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-12 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_9.jpg') }}" onclick="openModal();currentSlide(3)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-6 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_PE_1.jpg') }}" onclick="openModal();currentSlide(4)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-6 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_PE_7.jpg') }}" onclick="openModal();currentSlide(5)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-12 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_PURE_6.jpg') }}" onclick="openModal();currentSlide(6)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-6 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_PURE_8.jpg') }}" onclick="openModal();currentSlide(7)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-6 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_PE_9.jpg') }}" onclick="openModal();currentSlide(8)" class="img-fluid hover-shadow cursor"></figure>
                </div>
                <div class="col-12 mt-3 hover-shine">
                    <figure><img src="{{ asset('assets/sources-homepage/galerie/A110_PURE_4.jpg') }}" onclick="openModal();currentSlide(9)" class="img-fluid hover-shadow cursor"></figure>
                </div>
            </div>
            <!-- The Modal/Lightbox -->
            <div id="myModal" class="modal modal-style">
                <div class="modal-content mt-1">
                    <div class="mySlides mt-1">
                        <div class="numbertext">1 / 9</div>
                        <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_1.jpg') }}" style="width:55%">
                    </div>
                
                    <div class="mySlides mt-1">
                        <div class="numbertext">2 / 9</div>
                        <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_5.jpg') }}" style="width:55%">
                    </div>
                
                    <div class="mySlides mt-1">
                        <div class="numbertext">3 / 9</div>
                        <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_9.jpg') }}" style="width:55%">
                    </div>
                    <div class="mySlides mt-1">
                            <div class="numbertext">4 / 9</div>
                            <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_PE_1.jpg') }}" style="width:55%">
                    </div>
                    <div class="mySlides mt-1">
                            <div class="numbertext">5 / 9</div>
                            <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_PE_7.jpg') }}" style="width:55%">
                    </div>        
                    <div class="mySlides mt-1">
                            <div class="numbertext">6 / 9</div>
                            <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_PURE_6.jpg') }}" style="width:55%">
                    </div>
                    <div class="mySlides mt-1">
                            <div class="numbertext">7 / 9</div>
                            <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_PURE_8.jpg') }}" style="width:55%">
                    </div>
                    <div class="mySlides mt-1">
                            <div class="numbertext">8 / 9</div>
                            <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_PE_9.jpg') }}" style="width:55%">
                    </div>
                    <div class="mySlides mt-1">
                            <div class="numbertext">9 / 9</div>
                            <img class="d-block mx-auto" src="{{ asset('assets/sources-homepage/galerie/A110_PURE_4.jpg') }}" style="width:55%">
                    </div>
                <!-- Next/previous controls -->
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>
                <span class="close cursor" onclick="closeModal()">&times;</span>
                                        
                <div class="row mt-1 mb-1">
                <!-- Thumbnail image controls -->
                    <div class="col-1 ml-4">
                    <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_1.jpg') }}" onclick="currentSlide(1)" alt="Vue-route" width="100px">
                    </div>

                    <div class="col-1 ml-3">
                    <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_5.jpg') }}" onclick="currentSlide(2)" alt="Monaco" width="100px">
                    </div>

                    <div class="col-1 ml-3">
                    <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_LEGENDE_9.jpg') }}" onclick="currentSlide(3)" alt="Monaco-ville" width="100px">
                    </div>

                    <div class="col-1 ml-3">
                    <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_PE_1.jpg') }}" onclick="currentSlide(4)" alt="Route-enneigée" width="100px">
                    </div>
                    <div class="col-1 ml-3">
                        <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_PE_7.jpg') }}" onclick="currentSlide(5)" alt="Route-buche" width="100px">
                    </div>
                    <div class="col-1 ml-3">
                        <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_PURE_6.jpg') }}" onclick="currentSlide(6)" alt="Route-montagneuse" width="100px">
                    </div>
                    <div class="col-1 ml-3">
                        <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_PURE_8.jpg') }}" onclick="currentSlide(7)" alt="La vie à la montagne" width="100px">
                    </div>
                    <div class="col-1 ml-3">
                        <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_PE_9.jpg') }}" onclick="currentSlide(8)" alt="Alpine" width="100px">
                    </div>
                    <div class="col-1 ml-3">
                        <img class="demo cursor" src="{{ asset('assets/sources-homepage/galerie/A110_PURE_4.jpg') }}" onclick="currentSlide(9)" alt="Nuit-Alpine" width="100px">
                    </div>
                </div>    
                </div>
            </div>
        </div>
    </section>
