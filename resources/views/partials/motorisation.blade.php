 <!--Section Motorisation-->
 <section id="motorisation" class="pt-5 bg-blue">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center pt-5 text-white writing ">Motorisation</h1>
                <p class="text-justify pt-2 lead">Placé en position centrale arrière, le moteur turbocompressé 4
                    cylindres fait battre le cœur de l’A110. Réglé par les ingénieurs Alpine pour offrir une meilleure
                    réponse à l'accélération, le moteur à injection directe 1,8 L. joue la carte de la performance. La
                    signature sonore sportive ajoute quant à elle une part d’émotion.
                </p>
                <hr>
            </div>
            <div class="col-sm-6 pt-3">
                <h3 class="text-white ">Performances</h3>
                <p>La légèreté de la structure associée à un moteur turbocompressé font de l’A110 un modèle ultra
                    performant doté d’un rapport poids/puissance de 4,29 kg/ch. La preuve en est, l’A110 peut passer de
                    0 à 100 km/h en seulement 4,5 secondes.</p>
            </div>
            <div class="col-sm-6 pt-3">
                <h3 class="text-white ">Transmission</h3>
                <p>Assuré par une boîte à double embrayage, le système de transmission garantit des passages de vitesse
                    fluides et rapides. Les palettes en aluminium permettent au conducteur un contrôle parfait du
                    véhicule.
                </p>
            </div>
            <div class="col-12 mb-4 embed-responsive embed-responsive-16by9">
                <video autoplay loop>
                    <source src="{{ asset('assets/sources-homepage/motorisation/MOTEUR_CINEMAGRAPH-.mov') }}">
                </video>
            </div>
        </div>
</section>
<!--end of section-->