@extends('layout')
@section('title', 'Configurateur')
@section('style')
<style>

.pure {
    transition: 2s;
}
.legende {
    transition: 2s;
}
.pure:hover {
    box-shadow: 2px 1px 1px lightslategrey;
    transform: scale(1.1);

}
.pure:hover ~ .legende {
    -webkit-filter: blur(100px);
    filter: blur(100px);
}

.legende:hover {
    box-shadow: 2px 1px 1px lightslategrey;
    transform: scale(1.1);

}

.legende:hover ~ .pure {
    -webkit-filter: blur(100px);
    filter: blur(100px);

}
</style>
@endsection
@section('content')
<div id="app">
<div class="container-fluid mt-5 bg-dark pb-4">
    <div class="row">
        <div class="col-5 mt-5">
            <div class="card mb-5 bg-secondary text-white text-center pure">
                    <img src="{{  asset('assets/configurateur/modele/selection/pure.png') }}" class="card-img-top" alt="Responsive image">
                <div class="card-body">
                    <h5 class="card-title">A110 Pure</h5>
                    <p class="text-center">54 700€</p>
                    <a href="{{ route('pure') }}" class="btn btn-info">Configurer ce modèle</a>
                </div>
            </div>
        </div>
        <div class="col-2 mt-5 text-center text-white">
           <h2 class="my-auto"><br><br>- OU -<br> </h2>
        </div>
    <div class="col-5 mt-5 text-white">
            <div class="card mb-5 text-center bg-secondary legende">
                    <img src="{{  asset('assets/configurateur/modele/selection/legende.png') }}" class="card-img-top" alt="Responsive image">
                <div class="card-body">
                    <h5 class="card-title">A110 Légende</h5>
                    <p class="text-center">58 500€</p>
                    <a href="{{ route('legende') }}" class="btn btn-info">Configurer ce modèle</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    var app = new Vue({
  el: '#app',
  data: {
    message: 'Wesh Vue!'
  }
})

</script>
@endsection
