@extends('layout')
@section('title', 'Légende')
@section('style')
    <style>
        .color {
            width: 50px;
        }
        .jantes {
            width: 200px;
        }

        .sellerie {
            width: 350px;
        }

        .equipement {
            width: 200px;
            justify-content: space-around;

        }

        .pointer {
            cursor: pointer;
        }
        .block {
            display: block;
        }
        .none {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div id="app">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-12 col-md-6 mt-5 text-center">
                    <h1>A110 Légende</h1>
                    <hr>
                    <div id="carouselExampleIndicators" class="carousel slide mb-5" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="3" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="4" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="5" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="6" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="7" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="8" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="9" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="10" class="bg-dark"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="11" class="bg-dark"></li>
                        </ol>
                        <div class="carousel-inner" >
                            <div class="carousel-item active">
                                <img class="d-block w-100" :src='legende[0]'  alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" :src="legende[1]" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" :src="legende[2]" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" :src="legende[3]" alt="Fourth slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" :src="legende[4]" alt="Fifth slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" :src="sellerieDefault[0]" alt="Sixth slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" :src="sellerieDefault[1]" alt="Seventh slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" :src="sellerieDefault[2]" alt="Eighth slide">
                            </div> <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('assets/configurateur/interieurs/vues-avant/cockpit-1.jpg') }}" alt="cockpit-1">
                            </div> <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('assets/configurateur/interieurs/vues-avant/cockpit-2.jpg') }}" alt="cockpit-2">
                            </div> <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('assets/configurateur/interieurs/vues-avant/cockpit-3.jpg') }}" alt="cockpit-3">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon bg-dark" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon bg-dark" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6 mt-5">
                    <h1>Configurateur</h1>
                    <hr>
                    <h3>Couleurs</h3>
                    <a class="pointer" v-on:click="whiteColor" data-name="Carosserie blanche"><img title="Blanc Glacier - 0€" src="{{ asset('assets/configurateur/couleurs/selection/blanc.jpg') }}" class="img-fluid color" alt="voiture blanche"></a>
                    <a class="pointer" v-on:click="blueColor" data-name="Carosserie bleu Alpine"><img title="Bleu Alpine - 1800€" src="{{ asset('assets/configurateur/couleurs/selection/bleu.jpg') }}" class="img-fluid color" alt="voiture bleu"></a>
                    <a class="pointer" v-on:click="blackColor" data-name="Carosserie noire"><img title="Noir Profond- 840€" src="{{ asset('assets/configurateur/couleurs/selection/noir.jpg') }}" class="img-fluid color" alt="voiture noir"></a>
                    <h3>Sellerie</h3>
                    <a class="pointer" v-on:click="noir" data-name="Intérieur cuir noir"><img title="Siège cuir noir" src="{{ asset('assets/configurateur/interieurs/selection/cuir-noir.jpg') }}" class="img-fluid sellerie" alt="cuir noir"></a>
                    <a class="pointer" v-on:click="brun" data-name="Intérieur cuir brun"><img src="{{ asset('assets/configurateur/interieurs/selection/cuir-brun.jpg') }}" class="img-fluid sellerie" title="Siège cuir brun" alt="cuir brun"></a>
                </div>
            </div>
            <div class="container text-center">
                <div class="row pb-2">
                    <div class="col-12">
                        <h2>Equipements</h2>
                        <hr>
                        <h5 class="mb-3">Conduite</h5>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/conduite/aide-stationnement-ar.jpg') }}" class="img-fluid equipement" alt="aide stationnement ar"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/conduite/aide-stationnement-av-ar.jpg') }}" class="img-fluid equipement" alt="aide stationnement av-ar"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/conduite/camera-recul.jpg') }}" class="img-fluid equipement" alt="camera recul"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/conduite/echappement-sport.jpg') }}" class="img-fluid equipement" alt="echappement sport"></a>
                        <hr>
                        <h5 class="mb-3">Confort</h5>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/confort/pack-rangement.jpg') }}" class="img-fluid equipement" alt="rangement"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/confort/regul-limit-vitesse.jpg') }}" class="img-fluid equipement" alt="limitateur"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/confort/retro-ext-chaffant.jpg') }}" class="img-fluid equipement" alt="retro ext chauffant"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/confort/retro-int-electrochrome.jpg') }}" class="img-fluid equipement" alt="retro int electrochrome"></a>
                        <hr>
                        <h5 class="mb-3">Design</h5>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/design/pack-heritage.jpg') }}" class="img-fluid equipement" alt="pack heritage"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/design/repose-pied-alu.jpg') }}" class="img-fluid equipement" alt="repose pied alu"></a>
                        <hr>
                        <h5 class="mb-3">Media et navigation</h5>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/media et navigation/alpine-metrics.jpg') }}" class="img-fluid equipement" alt="alpine metrics"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/media et navigation/audio-focal.jpg') }}" class="img-fluid equipement" alt="audio focal"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/media et navigation/audio-premium.jpg') }}" class="img-fluid equipement" alt="audio premium"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/media et navigation/audio-standard.jpg') }}" class="img-fluid equipement" alt="audio standard"></a>
                        <hr>
                        <h5 class="mb-3">Personnalisation exterieure</h5>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/personnalisation exterieure/etrier-bleu.jpg') }}" class="img-fluid equipement" alt="etrier bleu"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/personnalisation exterieure/etrier-gris.jpg') }}" class="img-fluid equipement" alt="etrier gris"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/personnalisation exterieure/logo-alpine.jpg') }}" class="img-fluid equipement" alt="logo alpine"></a>
                        <hr>
                        <h5 class="mb-3">Personnalisation interieure</h5>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/personnalisation interieure/logo-volant.jpg') }}" class="img-fluid equipement" alt="logo volant"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/personnalisation interieure/pack-carbone.jpg') }}" class="img-fluid equipement" alt="pack carbone"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/personnalisation interieure/pedal-alu.jpg') }}" class="img-fluid equipement" alt="pedal alu"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/personnalisation interieure/siege-chauffant.jpg') }}" class="img-fluid equipement" alt="siege chauffant"></a>
                        <hr>
                        <h5 class="mb-3">Securité</h5>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/securite/aide-freinage-durgence.jpg') }}" class="img-fluid equipement" alt="aide freinage d'urgence"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/securite/airbag.jpg') }}" class="img-fluid equipement" alt="airbag"></a>
                        <a class="pointer" v-on:click="" data-name=""><img src="{{ asset('assets/configurateur/equipements/categories/securite/freinage-haute-perf.jpg') }}" class="img-fluid equipement" alt="freinage haute perf"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        new Vue({
            el: '#app',
            data : {
                /*Slide par défaut*/
                legende: [
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-1.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-2.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-3.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-4.jpg",
                    "../assets/configurateur/jantes/vues/couleur-blanc_jante-legende (2).jpg",
                ],
                /*slide voiture blanche standard*/
                legendeWhite: [
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-1.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-2.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-3.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-4.jpg",
                    "../assets/configurateur/jantes/vues/couleur-blanc_jante-legende (2).jpg",
                ],
                /*Slide voiture bleue standard*/
                legendeBlue: [
                    "../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-1.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-2.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-3.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-4.jpg",
                    "../assets/configurateur/jantes/vues/couleur-bleu_jante-legende (3).jpg",
                ],
                /*Slide voiture noire standard*/
                legendeBlack: [
                    "../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-1.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-2.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-3.jpg",
                    "../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-4.jpg",
                    "../assets/configurateur/jantes/vues/couleur-noir_jante-legende (1).jpg",
                ],

                /*Slide Sellerie*/
                /*Sellerie noire*/

                sellerieDefault: [
                    "../assets/configurateur/interieurs/vues/cuir-noir-1.jpg",
                    "../assets/configurateur/interieurs/vues/cuir-noir-2.jpg",
                    "../assets/configurateur/interieurs/vues/cuir-noir-3.jpg",
                ],

                sellerieBlack: [
                    "../assets/configurateur/interieurs/vues/cuir-noir-1.jpg",
                    "../assets/configurateur/interieurs/vues/cuir-noir-2.jpg",
                    "../assets/configurateur/interieurs/vues/cuir-noir-3.jpg",
                ],
                sellerieBrun: [
                    "../assets/configurateur/interieurs/vues/cuir-brun-1.jpg",
                    "../assets/configurateur/interieurs/vues/cuir-brun-2.jpg",
                    "../assets/configurateur/interieurs/vues/cuir-brun-3.jpg",
                ],

            },
            methods: {
                whiteColor() {
                    this.legende = this.legendeWhite;

                },
                blackColor() {
                    this.legende = this.legendeBlack;

                },
                blueColor() {
                    this.legende = this.legendeBlue;

                },
                noir() {
                    this.sellerieDefault = this.sellerieBlack
                },
                brun() {
                    this.sellerieDefault = this.sellerieBrun
                },
            }
        })


    </script>

@endsection
