@extends('layout')
@section('title', 'Page d\'accueil')
@section('style')
<style>
/* Animate h1 header ecran lg */
.main {
    position: absolute;
        top: 85%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

        /* 1er texte haut */
    .main-heading {
    color: #01BEFE;
    }

    .main-heading-primary {
        display: block;
        font-size: 5.0rem;
        font-weight: 400;
        letter-spacing: 28px;
        animation: moveInLeft 5s linear infinite alternate;
        padding-top: 3%;
        text-shadow: black 2px 0 10px;
    }

      /* 2eme texte bas  */
    .main-heading-secondary {
        display: block;
        font-size: 3.1rem;
        font-weight: bold;
        text-align: center;
        letter-spacing: 10px;
        animation: moveInRight 5s linear infinite alternate;
        text-shadow: black 2px 0 10px;
    }

    @keyframes moveInLeft {
        0% {
        opacity: 0;
        transform: translateX(-100px);
        }

        80% {
        transform: translateX(10px);
        }

        100% {
        opacity: 1;
        transform: translate(0);
        }
    }

    @keyframes moveInRight {
        0% {
        opacity: 0;
        transform: translateX(100px);
        }

        80% {
        transform: translateX(-10px);
        }

        100% {
        opacity: 1;
        transform: translate(0);
        }
    }

    /* FIN */


/* */
/* */
/* */
/* Responsive ecran Mobile h1 animate header*/
@media only screen and (max-width: 640px) {
    /* CSS mobile ici */
     /* Texte animate Téléphone*/
        /* Animate h1 header */
        .main {
            position: absolute;
                top: 80%;
                left: 49%;
                transform: translate(-50%, -50%);
            }
            
                /* 1er texte haut */
            .main-heading {
            color: #01BEFE;
            text-transform: uppercase;
            }
            
            
            .main-heading-primary {
                display: block;
                font-size: 2.3rem;
                font-weight: 400;
                letter-spacing: 12px;
                animation: moveInLeft 4s linear infinite alternate;
                padding-top: 8%;
                padding-bottom: 3%;
                text-shadow: black 2px 0 10px;

            }
            
            /* 2eme texte bas  */
            .main-heading-secondary {
                font-family: sans-serif;
                display: block;
                font-size: 1.5rem;
                font-weight: bold;
                text-align: center;
                letter-spacing: 8px;
                animation: moveInRight 4s linear infinite alternate;
                text-shadow: black 2px 0 10px;
            }
            
            @keyframes moveInLeft {
                0% {
                opacity: 0;
                transform: translateX(-100px);
                }
                
                80% {
                transform: translateX(10px);
                }
                
                100% {
                opacity: 1;
                transform: translate(0);
                }
            }
            
            @keyframes moveInRight {
                0% {
                opacity: 0;
                transform: translateX(100px);
                }
                
                80% {
                transform: translateX(-10px);
                }
                
                100% {
                opacity: 1;
                transform: translate(0);
                }
            }
            /* FIN H1 animate header */

            h1 {
                font-size: x-large !important;
            }
}


    /* The Modal (background) */
.modal-style {
    display: none;
    position: fixed;
    padding-top: 100px;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0,0,0,0.8);
}
  /* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    width: 90%;
    max-width: 1200px;
}

  /* The Close Button */
.close {
    color: black;
    position: absolute;
    top: 10px;
    right: 25px;
    font-size: 35px;
    font-weight: bold;
    transition: 0.6s ease;
    width: auto;
    padding: 16px;
    user-select: none;
    -webkit-user-select: none;


}
.close:hover,
.close:focus {
    color: #999;
    text-decoration: none;
    cursor: pointer;
    background-color: rgba(0, 0, 0, 0.8);

}
.cursor {
    cursor: pointer
}
  /* Hide the slides by default */
.mySlides {
    display: none;
}
  /* Next & previous buttons */
.prev,
.next {
    cursor: pointer;
    position: absolute;
    top: 50%;
    width: auto;
    padding: 16px;
    margin-top: -50px;
    color: white;
    font-weight: bold;
    font-size: 20px;
    transition: 0.6s ease;
    border-radius: 0 3px 3px 0;
    user-select: none;
    -webkit-user-select: none;
}
  /* Position the "next button" to the right */
.next {
    right: 0;
    border-radius: 3px 0 0 3px;
}

  /* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
    background-color: rgba(0, 0, 0, 0.8);
}
  /* Number text (1/3 etc) */
.numbertext {
    color: black;
    font-size: 12px;
    padding: 8px 12px;
    position: absolute;
    top: 0;
}
  /* Caption text */
.caption-container {
    text-align: center;
    background-color: rgba(0,0,0,0.8);
    padding: 2px 16px;
    color: white;
}
img.demo {
    opacity: 0.6;
    justify-content: space-around !important;

}
.active,
.demo:hover {
    opacity: 1;
}

img.hover-shadow {
    transition: 0.3s;
}

.hover-shadow:hover {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}


  /* Shine */
.hover-shine figure {
	position: relative;
}
.hover-shine figure::before {
	position: absolute;
	top: 0;
	left: -75%;
	z-index: 2;
	display: block;
	content: '';
	width: 50%;
	height: 100%;
	background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
	background: linear-gradient(to right, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
	-webkit-transform: skewX(-25deg);
	transform: skewX(-25deg);
}
.hover-shine figure:hover::before {
	-webkit-animation: shine .75s;
	animation: shine .75s;
}
@-webkit-keyframes shine {
	100% {
		left: 125%;
	}
}
@keyframes shine {
	100% {
		left: 125%;
	}
}

.writing {
    overflow: hidden; /* Ensures the content is not revealed until the animation */
    border-right: .15em solid orange; /* The typwriter cursor */
    white-space: nowrap; /* Keeps the content on a single line */
    margin: 0 auto; /* Gives that scrolling effect as the typing happens */
    letter-spacing: .15em; /* Adjust as needed */
    animation: 
    typing 3.5s steps(40, end) infinite,
    blink-caret .75s step-end infinite;
}

  /* The typing effect */
@keyframes typing {
    from { width: 0 }
    to { width: 100% }
}

  /* The typewriter cursor effect */
@keyframes blink-caret {
    from, to { border-color: transparent }
    /* 50% { border-color: black; } */
}
}
</style>
@endsection
    @section('content')
        @include('partials.agility')
        @include('partials.design')
        @include('partials.conception')
        @include('partials.motorisation')
        @include('partials.technology')
        @include('partials.inside')
        @include('partials.features')
        @include('partials.versions')
        @include('partials.gallery')
    @endsection

    @section('script')
        <script>
            // Carousel autoplay
                $('.carousel').carousel({
                    interval: 2000
                });
                
        // Open the Modal
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

// Close the Modal
function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}        
        </script>
@endsection